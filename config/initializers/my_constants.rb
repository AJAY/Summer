 PROJECT = {
  "sponsor"=>["Sponsor","string"],
  "due_date"=>["Deadline/ Submission Date","dateTime"],
  "funding"=>["Funding Document", "string"],
  "title"=>["Project Title", "string"],
  "start_date"=>["Start Date","dateTime"],
  "end_date"=>["End Date","dateTime"],
  "cost_share"=>["Cost Share","string"],
  "human_subjects"=>["Human Subjects","string"], 
  "clinical"=>["Clinical","string"],  
  "animals"=>["Animals","string"],  
  "waste"=>["Waste", "string"], 
  "ip_issues"=>["IP Issues", "string"], 
  "conflict"=>["Conflict", "string"], 
  "notes"=>["Additional Notes", "string"], 
  "ra_name"=>["RA Name", "string"], 
  "routing_no"=>["Proposal Routing #", "string"], 
  "proposal_status"=>["Proposal Status", "string"], 
  "award_status"=>["Award Status", "string"], 
  "solicitation"=>["Solicitation", "string"], 
  "fastlane_no"=>["FastLane #","string"],
  "pi_details"=>["PI","string"]
}

BUDGET = {
  "title"=>["Project Title", "string"],
  "budget"=>["Budget Required", "string"],
  "total_target"=>["Total Project Target", "string"],
  "annual_target"=>["Annual Target", "string"],
  "pi_co_effort"=>["PI/Co-PI Effort", "string"],
  "grad_student"=>["Graduate Students", "string"],
  "post_docs"=>["Post Docs.", "string"],
  "other_staff"=>["Other Staff", "string"],
  "material_supplies"=>["Material Supplies", "string"],
  "equipment_fees"=>["Equipment Fees", "string"],
  "consultants"=>["Consultants", "string"],
  "subgrant"=>["Subgrants/collaborators", "string"],
  "publications"=>["Publications", "string"],
  "foreign_travel"=>["Foreign Travel", "string"],
  "domestic_travel"=>["Domestic Travel", "string"],
  "domestic_description"=>["Domestic Travel Description", "string"],
  "non_capital_equipment"=>["Non-Capital Equipment", "string"],
  "non_capital_description"=>["Non-Capital Equipment Description", "string"],
  "other"=>["Additional Notes", "string"]
}

PERSON = {
  "title"=>["Project Title", "string"],
  "name"=>["Name", "string"],
  "contact"=>["Contact","string"],
  "category"=>["Position", "string"],
  "center"=>["Center","string"],
  "institution"=>["Institution", "string"],
  "leadPI"=>["Lead PI", "string"]
}
