class TrackController < ApplicationController
  
  before_action :authenticate_user!
  before_filter :init_list, :only => :new
  include ApplicationHelper

  def new
  	session[:track_id] ||= []
		session[:track_id].push(params[:id])
  end

  def list
  	@projects = Project.paginate(:page => params[:page], :per_page => 10)
  end

  def create
  	unless session[:track_id].include?(params[:id])
			flash[:alert] = "Opps!!! Either your session expired or information has been updated. Please reload the page or if problem persist, contact IT support."
			redirect_to action: 'new', id: params[:id]
		else
			school = get_other_format_data(params[:school],params[:school_text])  	
			academic = get_other_format_data(params[:academic],params[:academic_text])  	
			ra_contact = get_other_format_data(params[:contact],params[:contact_text])  	
			gco = get_other_format_data(params[:gco],params[:gco_text]) 	
			doctype = get_other_format_data(params[:doctype],params[:doctype_text])
			costtype = get_other_format_data(params[:sharetype],params[:sharetype_text])
			
			if !params[:udeadline].blank?
				params[:udeadline] = get_date(params[:udeadline])
			end
			
			begin
				@project = Project.update_track(params, school, academic, ra_contact, gco, doctype, costtype)			
				project_id =  @project.id
				Medium.update_description(params[:fadesc],project_id)
				flash[:notice] = "Thank you.Your submissions has been updated successfully!"
				session[:track_id].delete(params[:id])
				redirect_to :action => 'show'
			rescue => e
				puts e.message
				flash[:alert] = "Opps!!! Someone has already updated the information. Please try agian. If problem persists, contact IT support."
				redirect_to action: 'new', id: params[:id]
			end
		end	
	end

	def init_list
		result = Common.get_project_track_data params[:id]
		@data_track = []
		result.each(:as => :hash) do |record| 
			@data_track.push (record)
		end

		result = Project.get_data
		data = {}
		result.each(:as => :hash) do |record| 
			record.each do |k,v|
				data[k] ||= []
				data[k].push(v) unless v.blank?
			end
		end
		@data = {}
		data.each do |k,v|
			@data[k] = v.uniq
		end
	end
end
