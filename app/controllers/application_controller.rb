class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:account_update) { |u| 
      u.permit(:password, :password_confirmation, :current_password) 
    }

    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:email, :cached_failed_attempts, :failed_attempts) }
  end

  def autocomplete
    @sponsors = Sponsor.where("name LIKE ?", "%#{params[:q]}%")
    respond_to do |format|
      format.html
      format.json { 
        render json: @sponsors.map(&:name)
      }
    end
  end

  private
  def get_format_data(cs,fnd)
  	costshare = ""
  	if cs == "yes"
  		costshare = fnd
  	elsif cs == "no"
  		costshare = "no"
  	end
  	return costshare
  end
  
  def get_other_format_data(sp,sptxt)
  	sponsor = ""
  	if sp == "other"
  		sponsor = sptxt
  	else
  		sponsor = sp
  	end
  	return sponsor
  end
  
  def get_date dt
  	return Date.strptime(dt, "%m-%d-%Y").to_s
  end
  
  def write_media media_name, io
  	url = Rails.root.join('public', 'uploads', media_name)
  	File.open(url, 'wb') do |file|
  		file.write(io.read)
  	end
  	return url
  end

  def authenticate_active_admin_user!
    authenticate_user!
    unless current_user.role?(:administrator)
        flash[:alert] = "You are not authorized to access this resource!"
        redirect_to root_path
    end
  end
end
