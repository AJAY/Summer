class FiltersController < ApplicationController
  
  before_action :authenticate_user!
  
  def new
  end

  def create

    if params[:fromDate].blank?
      params[:fromDate]=Date.today.at_beginning_of_month
    else
      params[:fromDate]=  Date.strptime(params[:fromDate], '%m-%d-%Y')
    end
    if params[:toDate].blank?
      params[:toDate]=Date.today
    else
      params[:toDate]=  Date.strptime(params[:toDate], '%m-%d-%Y')
    end

    @data_project = Common.get_project_data params[:fromDate],params[:toDate]

    if(params[:budget]=="budget")
      @data_budget = Common.get_budget_data params[:fromDate],params[:toDate]      
    end


    if(params[:pi_details]=="pi_details" || params[:copi_details]=="copi_details" ||params[:collaborators]=="collaborators" )
      category_type = ""
      if(params[:pi_details]=="pi_details" && params[:copi_details]=="copi_details" && params[:collaborators]=="collaborators" )
      else
        category_type = " and (" 
        if(params[:pi_details]=="pi_details")
          category_type += " ps.category = 'pi'"
        end 
        if(params[:copi_details]=="copi_details")
          category_type += " or " if category_type!=" and (" 
          category_type += " ps.category = 'co-pi'"
        end 
        if(params[:collaborators]=="collaborators")
          category_type += " or " if category_type!=" and (" 
          category_type += " ps.category = 'collaborators'"
        end 
        category_type += " )" 
      end

      @data_person = Common.get_person_data params[:fromDate],params[:toDate], category_type 
    end
    respond_to do |format|
      format.xls 
    end
  end
end
