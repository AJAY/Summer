class IformsController < ApplicationController

	before_action :authenticate_user!
	before_filter :init_list, except: [:create, :show]
	include ApplicationHelper

	def new
		session[:project_id] ||= []
		session[:project_id].push(params[:id])
	end

	def show
	end

	def create
		unless session[:project_id].include?(params[:id])
			flash[:alert] = "Opps!!! Either your session expired or information has been updated. Please reload the page or if problem persist, contact IT support."
			redirect_to action: 'new', id: params[:id]
		else
			costshare = get_format_data(params[:costshare], params[:funding])
			conflict = get_format_data(params[:conflict], params[:conflict_person])
			sponsor = params[:sponsor]
			begin
		
				if !params[:due_Date].blank?
					params[:due_Date] = get_date(params[:due_Date])
				end

				if !params[:startDate].blank?
					params[:startDate] = get_date(params[:startDate])
				end

				if !params[:endDate].blank?
					params[:endDate] = get_date(params[:endDate])
				end
				
				if !params[:file_name].blank?
					uploaded_io = params[:file_name]
					if(["application/x-ms-dos-executable","application/octet-stream", "application/x-msdownload", "application/exe", "application/x-exe", "application/dos-exe", "vms/exe", "application/x-winexe", "application/msdos-windows", "application/x-msdos-program"].include?(uploaded_io.content_type))
						raise "error"
					end
					media_name = (Time.now.to_i).to_s+uploaded_io.original_filename
					media_type = uploaded_io.content_type
					url = write_media(media_name, uploaded_io)	
					media_name = uploaded_io.original_filename
				elsif !params[:remote_url].blank?
					url = params[:remote_url]
				end
				@project = Project.update_project(params, sponsor,costshare, conflict)
				Sponsor.insert_sponsor(sponsor)
				project_id =  @project.id
				Budget.update_budget(params, project_id)	
				unless(params[:file_name].blank? and params[:remote_url].blank?)
					Medium.update_medium(params[:funding_option],media_name,media_type,url.to_s,project_id)
				end
				Invite.remove_entry project_id
				params[:collaborator].each_with_index do |c, index|
					category, name, center = get_person_detail(index,params[:collaborator_text][index],params[:center_text][index],params[:center][index], c)
					@person =  Person.insert_internal(name, category, center)
					Invite.update_invite(project_id, @person.id,params[:allocation][index])
				end
				unless params[:external_name].blank?
					params[:external_name].each_with_index do |c, index|	
						if not(c.blank?)
							@person = Person.insert_external(params[:external_name][index], params[:external_contact][index], params[:external_institution][index], params[:external_PI][index])
							Invite.update_invite(project_id, @person.id, 0)
						end
					end
				end
				# project_id = 1
				# puts url = request.host_with_port + "/iforms/"+project_id.to_s
				# ApplicationMailer.mailer(url).deliver_now
				flash[:notice] = "Thank you.Your submissions has been updated successfully!"
				session[:project_id].delete(params[:id])
				redirect_to :action => 'show'
			rescue => e
				puts e.message
				flash[:alert] = "Opps!!! Something goes wrong. Please try agian. If problem persists, contact IT support."
				redirect_to action: 'new', id: params[:id]
			end
		end
	end

	def init_list
		@people = Person.get_person
		@center = Person.get_center                   
		
		result = Common.get_project_info params[:id]
		@data_project = []
		result.each(:as => :hash) do |record| 
			@data_project.push (record)
		end

		result = Common.get_budget_info params[:id]
		@data_budget = []
		result.each(:as => :hash) do |record| 
			@data_budget.push (record)
		end

		result = Common.get_person_info params[:id]
		@data_person = []
		result.each(:as => :hash) do |record| 
			@data_person.push (record)
		end
	end
end
