class IntakesController < ApplicationController

	before_filter :init_list, :only => :new
	include ApplicationHelper

	def new  	
	end

	def show
	end

	def create
		costshare = get_format_data(params[:costshare], params[:funding])
		conflict = get_format_data(params[:conflict], params[:conflict_person])
		sponsor = params[:sponsor]  	
		begin
			if !params[:due_Date].blank?
				params[:due_Date] = get_date(params[:due_Date])
			end

			if !params[:startDate].blank?
				params[:startDate] = get_date(params[:startDate])
			end

			if !params[:endDate].blank?
				params[:endDate] = get_date(params[:endDate])
			end
			if !params[:file_name].blank?
				uploaded_io = params[:file_name]
				if(["application/x-ms-dos-executable","application/octet-stream", "application/x-msdownload", "application/exe", "application/x-exe", "application/dos-exe", "vms/exe", "application/x-winexe", "application/msdos-windows", "application/x-msdos-program"].include?(uploaded_io.content_type))
					raise "error"
				end
				media_name = (Time.now.to_i).to_s+uploaded_io.original_filename
				media_type = uploaded_io.content_type
				url = write_media(media_name, uploaded_io)	
				media_name = uploaded_io.original_filename
			elsif !params[:remote_url].blank?
				url = params[:remote_url]
			end		

			@project = Project.insert_project(params, sponsor,costshare, conflict)
			Sponsor.insert_sponsor(sponsor)
			project_id =  @project.id
			@budget = Budget.insert_budget(params, project_id)	
			@media = Medium.insert_medium(params[:funding_option],media_name,media_type,url,project_id)
			params[:collaborator].each_with_index do |c, index|
				category, name, center = get_person_detail(index,params[:collaborator_text][index],params[:center_text][index],params[:center][index], c)
				@person = Person.insert_internal(name, category, center)
				@invite = Invite.insert_invite(project_id, @person.id,params[:allocation][index])
			end

			params[:external_name].each_with_index do |c, index|	
			if not(c.blank?)
				@person = Person.insert_external(params[:external_name][index], params[:external_contact][index], params[:external_institution][index], params[:external_PI][index])
					@invite = Invite.insert_invite(project_id, @person.id, 0)
				end
			end
			# project_id = 1
			puts i_url = "https://"+request.host_with_port + "/iforms/"+project_id.to_s
			# Thread.new do
			params[:file_name]=""
			ApplicationMailer.delay.mailer(i_url, costshare, conflict, sponsor, params, url, media_name)
			# ApplicationMailer.mailer(i_url, costshare, sponsor, params, url, media_name).deliver_now
			# end
			flash[:notice] = "Thank you.Your submissions has been sent successfully!"
			redirect_to :action => 'show'
		rescue => e
			init_list
			puts e.message
			flash[:alert] = "Opps!!! Something goes wrong. Please try agian. If problem persists, contact IT support."
			render :action => 'new'
		end
	end

	def init_list
		@people = Person.get_person
		@center = Person.get_center                  
	end
end
