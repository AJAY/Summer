ActiveAdmin.register User do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params [:email, :encrypted_password, :password, :password_confirmation, :role,:reset_password_token, :reset_password_sent_at, :remember_created_at, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip] 

form do |f|
  f.inputs "User" do
    f.input :email
    f.input :password
    f.input :password_confirmation
    f.input :role
  end
  f.actions
end

# permit_params do
#   permitted = [:permitted, :attributes]
#   debugger
#   permitted << :other if resource.something?
#   permitted
# end

end
