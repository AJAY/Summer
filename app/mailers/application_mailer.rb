class ApplicationMailer < ActionMailer::Base
	add_template_helper(ApplicationHelper)

	def mailer(i_url, costshare, conflict, sponsor, params, url, media_name)
		@url = i_url
		@costshare = costshare
		@conflict = conflict
		@sponsor = sponsor
		@params = params
		@media_url = url
		@media_name = media_name
		list = YAML::load(File.open("#{Rails.root.to_s}/config/email.yml"))
		unless media_name.blank?
			attachments[media_name] = File.read(url)
		end
		mail(to: list["receivers"], subject: 'New RA Grant Application')
	end
end