class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :timeoutable, :registerable,
         :recoverable, :rememberable, :validatable, :lockable, :timeout_in => 15.minutes

  def self.logins_before_captcha
    3
  end
  
  def role?(r)
      role.include? r.to_s if role
  end
end
