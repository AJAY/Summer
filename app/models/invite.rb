class Invite < ActiveRecord::Base
	belongs_to :project
	belongs_to :person

	def self.insert_invite project_id, person_id, allocation
		self.create!(
			project_id: project_id,
			person_id: person_id,
			allocation: allocation,
			deleted: false)
	end

	def self.remove_entry project_id
		self.where(:project_id => project_id ).update_all(deleted: true)
	end

	def self.update_invite project_id, person_id, allocation
		i = self.find_or_create_by(project_id: project_id,person_id: person_id)
		sql = ActiveRecord::Base.send(:sanitize_sql_array, ["allocation = ? WHERE project_id = ? and person_id = ?",allocation,project_id,person_id])
		ActiveRecord::Base.connection.execute("UPDATE invites SET deleted = 0, #{sql}")
	end
	
	# Invite.remove_entry project_id where(["person_id in (?) and project_id NOT IN (?)",[1,2], [3,9,23]])
end
