class Person < ActiveRecord::Base
	has_many :invites
	has_many :projects, through: :invites

	def self.insert_internal name, category, center
		self.find_or_create_by(
			name: name,
			contact: nil,
			institution: nil,
			leadPI: nil,
			category: category,
			center: center,
			deleted: false)
	end

	def self.insert_external name, contact, institution, leadPI
		self.find_or_create_by(
			name: name,
			contact: contact,
			institution: institution,
			leadPI: leadPI,
			category: "external collaborators",
			center: nil,
			deleted: false)
	end

	def self.get_person
		sql = "select distinct p.name from people p where (p.category = 'pi' or p.category = 'co-pi') and p.name is not null and p.name <> ''"
		data_people = ActiveRecord::Base.connection.execute(sql,:as => :hash)
		people = []
		data_people.each(:as => :hash) do |record| 
			people.push (record["name"])
		end
		return people
	end

	def self.get_center
		sql = "select distinct p.center from people p where p.center is not null and p.center <> ''"
		data_center = ActiveRecord::Base.connection.execute(sql,:as => :hash)
		center = []
		data_center.each(:as => :hash) do |record| 
			center.push (record["center"])
		end
		return center
	end

end
