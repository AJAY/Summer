class Common < ActiveRecord::Base
	def self.get_project_info project_id
		
		sql = ActiveRecord::Base.send(:sanitize_sql_array, ["p.id = ?",project_id])
		data_project = ActiveRecord::Base.connection.execute("select p.title, p.due_at as due_date, 
			p.start_date, p.end_date, p.sponsor, p.cost_share,p.human_subject as human_subjects,
			p.clinical_trials as clinical, p.animals, p.waste, p.ip_issues,
			p.conflict_person as conflict, p.RA_name as ra_name, p.proposal_status, p.award_status, p.solicitation,
			p.routing as routing_no, p.fastlane as fastlane_no, p.additional_notes as notes, p.lock_version,
			m.filename, m.option , m.url as funding
			from projects p 
			LEFT JOIN media m on m.project_id = p.id  
			where #{sql}")
	end

	def self.get_project_data from_date, to_date
		sql = ActiveRecord::Base.send(:sanitize_sql_array, ["(p.due_at between ? and ?) ","#{from_date.strftime('%Y-%m-%d')} 00:00:00","#{to_date.strftime('%Y-%m-%d')} 23:59:59"])
		data_project = ActiveRecord::Base.connection.execute("select p.id, p.title, p.due_at as due_date, p.start_date, p.end_date, p.sponsor, p.cost_share,
	      p.human_subject as human_subjects, p.clinical_trials as clinical, p.animals, p.waste, p.ip_issues,
	      p.conflict_person as conflict, p.RA_name as ra_name, p.proposal_status, p.award_status, p.solicitation,
	      p.routing as routing_no, p.fastlane as fastlane_no, p.additional_notes as notes, m.url as funding, ps.name as pi_details
	      from projects p 
	      LEFT JOIN media m on m.project_id = p.id  
	      LEFT JOIN invites i on i.project_id = p.id
          LEFT JOIN people ps on ps.id = i.person_id
          where i.deleted = 0 and ps.category = 'pi' and #{sql}
	      ORDER BY p.due_at;")
	end

	def self.get_budget_info project_id
		sql = ActiveRecord::Base.send(:sanitize_sql_array, ["p.id = ?",project_id])
		data_budget = ActiveRecord::Base.connection.execute(
			"select b.budget_option as budget,b.total_target,b.annual_target,
			b.pi_co_effort,b.grad_student,b.post_docs,b.other_staff,b.material_supplies,
			b.equipment_fees,b.consultants,b.subgrant,b.publications,b.foreign_travel,
			b.domestic_travel,b.domestic_description,b.non_capital_equipment,
			b.non_capital_description,b.other
			from projects p 
			LEFT JOIN budgets b on b.project_id = p.id
			where  #{sql}")
	end

	def self.get_budget_data from_date,to_date
		sql = ActiveRecord::Base.send(:sanitize_sql_array, ["(p.due_at between ? and ?) ","#{from_date.strftime('%Y-%m-%d')} 00:00:00","#{to_date.strftime('%Y-%m-%d')} 23:59:59"])
		data_budget = ActiveRecord::Base.connection.execute("select p.id, p.title, p.due_at as due_date, b.budget_option as budget,
        b.total_target,b.annual_target,b.pi_co_effort,b.grad_student,b.post_docs,b.other_staff,
        b.material_supplies,b.equipment_fees,b.consultants,b.subgrant,b.publications,b.foreign_travel,
        b.domestic_travel,b.domestic_description,b.non_capital_equipment,b.non_capital_description,b.other
        from projects p 
        LEFT JOIN budgets b on b.project_id = p.id
        where #{sql} 
        ORDER BY p.due_at;")
	end

	def self.get_person_info project_id
		sql = ActiveRecord::Base.send(:sanitize_sql_array, ["p.id = ?",project_id])
		data_person = ActiveRecord::Base.connection.execute(
			"select ps.name,ps.contact,ps.institution,ps.leadPI,ps.category,ps.center,i.allocation
			from projects p 
			LEFT JOIN invites i on i.project_id = p.id
			LEFT JOIN people ps on ps.id = i.person_id
			where i.deleted= 0 and  #{sql}")
	end
	
	def self.get_person_data from_date,to_date, category_type
		sql = ActiveRecord::Base.send(:sanitize_sql_array, ["(p.due_at between ? and ?) ","#{from_date.strftime('%Y-%m-%d')} 00:00:00","#{to_date.strftime('%Y-%m-%d')} 23:59:59"])
		data_person = ActiveRecord::Base.connection.execute("select p.id, p.title,  p.due_at as due_date,
        ps.name,ps.contact,ps.institution,ps.leadPI,ps.category,ps.center,i.allocation
        from projects p 
        LEFT JOIN invites i on i.project_id = p.id
        LEFT JOIN people ps on ps.id = i.person_id
        where i.deleted= 0 and #{sql} #{category_type}
        ORDER BY p.due_at;")
	end

	def self.get_project_track_data project_id
		sql = ActiveRecord::Base.send(:sanitize_sql_array, ["p.id = ?",project_id])
		data_track = ActiveRecord::Base.connection.execute("select p.id, p.title, p.proposal_no, p.school,  p.due_at as due_date,
        ps.name as pi_name, p.start_date, p.end_date, p.sponsor, p.cost_share,
		p.RA_name as ra_name, p.RA_contact as ra_contact, p.proposal_status, p.academic, p.prime_sponsor, p.gco,
		p.account_no, p.cancellation_reason, p.document_type, p.notification, p.waiver,
		p.unitdeadline, p.cost_type, p.highlights, p.lock_version, b.total_target,
		m.id as media_id, m.filename , m.url as media_url, m.description
        from projects p 
        LEFT JOIN budgets b on b.project_id = p.id
        LEFT JOIN media m on m.project_id = p.id  
		LEFT JOIN invites i on i.project_id = p.id
        LEFT JOIN people ps on ps.id = i.person_id
        where #{sql} and i.deleted= 0 and ps.category='pi'")
	end
end
