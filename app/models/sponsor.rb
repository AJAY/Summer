class Sponsor < ActiveRecord::Base

	def self.insert_sponsor name
		self.find_or_create_by(
			name: name
		)
	end

end
