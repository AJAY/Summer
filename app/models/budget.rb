class Budget < ActiveRecord::Base
	belongs_to :project

	def self.insert_budget params, project_id
		self.create!(
			budget_option: params[:budget],
			project_id: project_id,
			total_target: params[:total_budget],
			annual_target: params[:annual_budget],
			pi_co_effort: params[:pi_co_effort],
			grad_student: params[:grad_student],
			post_docs: params[:post_docs],
			other_staff: params[:other_staff],
			material_supplies: params[:material_supplies],
			equipment_fees: params[:equipment_fees],
			consultants: params[:consultants],
			subgrant: params[:subgrant],
			publications: params[:publications],
			foreign_travel: params[:foreign_travel],
			domestic_travel: params[:domestic_travel],
			domestic_description: params[:domestic_description],
			non_capital_equipment: params[:non_capital_equipment],
			non_capital_description: params[:non_capital_description],
			other: params[:budget_other]
		)
	end

	def self.update_budget params, project_id
		self.where(:project_id => project_id ).update_all(
			budget_option: params[:budget],
			total_target: params[:total_budget],
			annual_target: params[:annual_budget],
			pi_co_effort: params[:pi_co_effort],
			grad_student: params[:grad_student],
			post_docs: params[:post_docs],
			other_staff: params[:other_staff],
			material_supplies: params[:material_supplies],
			equipment_fees: params[:equipment_fees],
			consultants: params[:consultants],
			subgrant: params[:subgrant],
			publications: params[:publications],
			foreign_travel: params[:foreign_travel],
			domestic_travel: params[:domestic_travel],
			domestic_description: params[:domestic_description],
			non_capital_equipment: params[:non_capital_equipment],
			non_capital_description: params[:non_capital_description],
			other: params[:budget_other]
		)
	end
end
