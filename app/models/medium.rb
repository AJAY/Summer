class Medium < ActiveRecord::Base
	belongs_to :project

	def self.insert_medium funding_option,media_name,media_type,url,project_id
		self.create!(
			option: funding_option,
			filename: media_name,
			content_type: media_type,
			url: url,
			project_id: project_id,
			deleted: false
		)
	end

	def self.update_medium funding_option,media_name,media_type,url,project_id
		self.where(:project_id => project_id ).update_all(
			option: funding_option,
			filename: media_name,
			content_type: media_type,
			url: url
		)
	end

	def self.update_description description,project_id
		self.where(:project_id => project_id ).update_all(
			description: description 
		)
	end
end
