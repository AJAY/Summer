class Project < ActiveRecord::Base
	has_one :budget
	has_one :medium
	has_one :track
	has_many :invites
	has_many :people, through: :invites


	def self.insert_project params, sponsor, costshare, conflict
		self.create!(
			title: params[:title],
			due_at: params[:due_Date],
			start_date: params[:startDate],
			end_date: params[:endDate],
			sponsor: sponsor,
			cost_share: costshare,
			human_subject: params[:human_subject],
			clinical_trials: params[:clinical_trials],
			animals: params[:animals],
			waste: params[:waste],
			ip_issues: params[:ip_issues],
			conflict_person: conflict,
			RA_name: params[:ra_name],
			proposal_status: params[:proposal_status],
			award_status: params[:award_status],
			solicitation: params[:solicitation],
			routing: params[:routing_no],
			fastlane: params[:fastlane_no],
			additional_notes: params[:notes])
	end

	def self.update_project params, sponsor, costshare, conflict
		self.update( params[:id],
			title: params[:title],
			due_at: params[:due_Date],
			start_date: params[:startDate],
			end_date: params[:endDate],
			sponsor: sponsor,
			cost_share: costshare,
			human_subject: params[:human_subject],
			clinical_trials: params[:clinical_trials],
			animals: params[:animals],
			waste: params[:waste],
			ip_issues: params[:ip_issues],
			conflict_person: conflict,
			RA_name: params[:ra_name],
			proposal_status: params[:proposal_status],
			award_status: params[:award_status],
			solicitation: params[:solicitation],
			routing: params[:routing_no],
			fastlane: params[:fastlane_no],
			additional_notes: params[:notes],
			lock_version: params[:lock_version])
	end

	def self.get_sponsor
		sql = "select distinct p.sponsor from projects p 
		where p.sponsor is not null and p.sponsor <> ''"
		return ActiveRecord::Base.connection.execute(sql,:as => :hash)
	end

	def self.get_data
		sql = "select distinct school, academic, gco, RA_name, RA_contact, document_type, 
		cost_type FROM projects"
		return ActiveRecord::Base.connection.execute(sql,:as => :hash)
	end

  def self.update_track(params, school, academic, ra_contact, gco, doctype, costtype)  	
		self.update(params[:id],
			proposal_no: params[:proposalno],
			school: school,
			academic: academic,
			prime_sponsor: params[:psname],
			RA_contact: ra_contact,
			gco: gco,
			proposal_status: params[:proposal_status],
			account_no: params[:acno],
			cancellation_reason: params[:cancel_reason],
			document_type: doctype,
			notification: params[:notification],
			unitdeadline: params[:udeadline],
			cost_type: costtype,
			waiver: params[:waiver],
			highlights: params[:highlights],
			lock_version: params[:lock_version])
	end
end
