module ApplicationHelper
	def get_person_detail index,coltx,ctx,ctr, c
  	category = "pi"
  	if(index > 0)
  		category = "co-pi"
  	end
  	name = c
  	if c == "other"
  		name = coltx
    end
  	center = ctr
    if ctr == "other"  	
  		center = ctx
  	end
  	return [category,name,center]
  end
end
