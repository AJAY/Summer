$(".iforms").ready(function() {
	if(typeof data_budget !== 'undefined' && typeof data_project !== 'undefined' && typeof data_person !== 'undefined'){
		$('input:radio[name="budget"]').filter('[value="'+data_budget[0]["budget"]+'"]').attr('checked', true).change();
		$("#budget input[name=annual_budget]").val(data_budget[0]["annual_target"]);
		$("#budget input[name=consultants]").val(data_budget[0]["consultants"]); 
		$("#budget input[name=equipment_fees]").val(data_budget[0]["equipment_fees"]); 
		$("#budget input[name=grad_student]").val(data_budget[0]["grad_student"]); 
		$("#budget input[name=post_docs]").val(data_budget[0]["post_docs"]); 
		$("#budget input[name=pi_co_effort]").val(data_budget[0]["pi_co_effort"]); 
		$("#budget input[name=total_budget]").val(data_budget[0]["total_target"]); 
		$("#budget input[name=subgrant]").val(data_budget[0]["subgrant"]); 
		$("#budget input[name=publications]").val(data_budget[0]["publications"]); 
		$("#budget input[name=material_supplies]").val(data_budget[0]["material_supplies"]); 
		$("#budget input[name=domestic_travel]").val(data_budget[0]["domestic_travel"]); 
		$("#budget input[name=non_capital_equipment]").val(data_budget[0]["non_capital_equipment"]); 
		$("#budget textarea[name=domestic_description]").val(data_budget[0]["domestic_description"]); 
		$("#budget input[name=foreign_travel]").val(data_budget[0]["foreign_travel"]); 
		$("#budget textarea[name=non_capital_description]").val(data_budget[0]["non_capital_description"]); 
		$("#budget input[name=other_staff]").val(data_budget[0]["other_staff"]); 
		$("#budget input[name=budget_other]").val(data_budget[0]["other"]); 
		
		$("input[name=sponsor]").val(data_project[0]["sponsor"]);
		$('input:radio[name="waste"]').filter('[value="'+data_project[0]["waste"]+'"]').attr('checked', true);
		$("input[name=title]").val(data_project[0]["title"]);

		$("input[name=due_Date]").val(date_format(data_project[0]['due_date']));
		$("input[name=endDate]").val(date_format(data_project[0]['end_date']));
		$("input[name=startDate]").val(date_format(data_project[0]['start_date']));
		$('input:radio[name="animals"]').filter('[value="'+data_project[0]["animals"]+'"]').attr('checked', true);
		$('input:radio[name="clinical_trials"]').filter('[value="'+data_project[0]["clinical"]+'"]').attr('checked', true);
		$('input:radio[name="human_subject"]').filter('[value="'+data_project[0]["human_subjects"]+'"]').attr('checked', true);
		$('input:radio[name="ip_issues"]').filter('[value="'+data_project[0]["ip_issues"]+'"]').attr('checked', true);
		$("select[name=award_status]").val(data_project[0]["award_status"]);

		if(data_project[0]["conflict"]==""){
		} 
		else if(data_project[0]["conflict"]=="no"){
			$('input:radio[name="conflict"]').filter('[value="no"]').attr('checked', true)
		}else{	
			$('input:radio[name="conflict"]').filter('[value="yes"]').attr('checked', true).change();
			$("input[name=conflict_person]").val(data_project[0]["conflict"]);
		}

		if(data_project[0]["cost_share"]==""){
		} 
		else if(data_project[0]["cost_share"]=="no"){
			$('input:radio[name="costshare"]').filter('[value="no"]').attr('checked', true)
		}else{	
			$('input:radio[name="costshare"]').filter('[value="yes"]').attr('checked', true).change();
			$("input[name=funding]").val(data_project[0]["cost_share"]);
		}

		$("input[name=fastlane_no]").val(data_project[0]["fastlane_no"]);
		$("textarea[name=notes]").val(data_project[0]["notes"]); 
		$("select[name=proposal_status]").val(data_project[0]["proposal_status"]);
		$("input[name=ra_name]").val(data_project[0]["ra_name"]);
		$("input[name=solicitation]").val(data_project[0]["solicitation"]);
		$("input[name=routing_no]").val(data_project[0]["routing_no"]);
		
		$.each(data_person, function( index, value ) {
		  if(value["category"]=="pi" || value["category"]=="co-pi"){
		  	var row = "<td>"+value["category"].toUpperCase()+" </td><td><select class='form-control collaborator' name='collaborator[]'>"
		    + "<option value= ''></option>";
		    
		    $.each(person, function(key,val){
		    	if(val == value["name"]){
		      		row += "<option selected='selected' value='"+ val + "'>" + val +"</option>";
		    	}else{
		      		row += "<option value='"+ val + "'>" + val +"</option>";
		    	}
		    }) 

		    row += "<option value= 'other'>Other</option>";
		    row += "</select><input type='text' name='collaborator_text[]' style='display:none;'"
		    + "placeholder='Name' class='form-control collaborator_name'/></td><td><select " 
		    + "class='form-control center' name='center[]'> <option value= ''></option> ";

		    $.each(center, function(key,val){
		    	if(val == value["center"]){
		      		row += "<option selected='selected' value='"+ val + "'>" + val +"</option>";
		    	}else{
		      		row += "<option value='"+ val + "'>" + val +"</option>";
		    	}
		    }) 

		    row += "<option value= 'other'>Other</option>"
		    row += "</select><input type='text' name='center_text[]' style='display:none;'"
		    + "placeholder='Center' class='form-control collaborator_center'/></td>"
		    + "<td><input name='allocation[]' type='text' value='"+value["allocation"]+"' class='input-small form-control' placeholder='Allocation %' "
		    + "></td><td><a class='pull-right btn btn-danger delete_row'>Delete</a>"
		    +"</td>";
		    $('#tab_logic').append('<tr>'+row+'</tr>');
		  
		  	
		  }else if(value["category"]=="external collaborators"){
		  	var row = "<td rowspan='4'><li></li></td> <td>Name</td><td><input name='external_name[]'"
		    + " type='text' placeholder='Name' class='form-control input-md' value='"+value["name"]+"' /> </td><td rowspan='4'> "
		    + " <a class='pull-right btn btn-danger delete_box'> Delete</a></td></tr><tr> <td> Contact </td>"
		    + " <td> <input type='text' name='external_contact[]' placeholder='Contact' class='form-control' value='"+value["contact"]+"' /> "
		    + "</td> </tr> <tr> <td> Which Institution is the Lead? </td> <td>"
		    + " <input type='text' name='external_institution[]' placeholder='Institution Name' value='"+value["institution"]+"' " 
		    + "class='form-control'/> </td> </tr> <tr> <td> Name of Lead PI at collaborating institutions? </td>" 
		    + "<td> <input type='text' name='external_PI[]' placeholder='Name of Lead PI' class='form-control' value='"+value["leadPI"]+"' />"
		    + "</td> </tr>"; 
		    $('#tab_col').append('<tr>'+row+'</tr>');

		  }
		});
		

		$(".new_file").click(function(){     
		  $("#file_upload").css({"display":"block", "margin-top":"10px"});
	      $("#remote_url").css("display", "none");
	      $("#file_upload_input").attr("data-validation", "mime size required");
	      $("#remote_url").removeAttr("data-validation");
	      $("#remote_url").val("");    
		});
 	}
});

function date_format(str){
	if (str==null){
		return '';
	}else{
		from = str.split("-");
		return(from[1]+"-"+from[2]+"-"+from[0]); 
	}
}