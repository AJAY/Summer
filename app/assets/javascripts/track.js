$(".track").ready(function() {
	if(typeof data_track !== 'undefined' ){
		$("input[name=title]").val(data_track[0]["title"]);
		$("input[name=proposalno]").val(data_track[0]["proposal_no"]);
		$("select[name=school]").val(data_track[0]["school"]);
		$("input[name=investigator]").val(data_track[0]["pi_name"]);
		$("select[name=academic]").val(data_track[0]["academic"]);
		$("input[name=sponsor]").val(data_track[0]["sponsor"]);
		$("input[name=psname]").val(data_track[0]["prime_sponsor"]);
		$("select[name=contact]").val(data_track[0]["ra_contact"]);
		$("select[name=gco]").val(data_track[0]["gco"]);
		$("select[name=proposal_status]").val(data_track[0]["proposal_status"]);
		$("input[name=acno]").val(data_track[0]["account_no"]);
		$("input[name=cancel_reason]").val(data_track[0]["cancellation_reason"]);
		$("input[name=fano]").val(data_track[0]["media_id"]);
		if(data_track[0]["filename"]){
			remote_url = data_track[0]["media_url"].split("/");  
			$("input[name=falink]").val(data_track[0]["filename"]);
			$("input[name=falink]").prop( "url", window.location.origin+"/uploads/"+remote_url[remote_url.length-1] );
		}else{
			$("input[name=falink]").val(data_track[0]["media_url"]);
			$("input[name=falink]").prop( "url", data_track[0]["media_url"] );
		}
		
		$("input[name=fadesc]").val(data_track[0]["description"]);
		$("select[name=doctype]").val(data_track[0]["document_type"]);
		$("input[name=notification]").val(data_track[0]["notification"]);
		$("input[name=udeadline]").val(date_format(data_track[0]['unitdeadline']));
		$("input[name=sdeadline]").val(date_format(data_track[0]['due_date']));
		$("input[name=budget]").val(data_track[0]["total_target"]);
		$("input[name=endDate]").val(date_format(data_track[0]['end_date']));
		$("input[name=startDate]").val(date_format(data_track[0]['start_date']));
		if(data_track[0]["cost_share"]!="" && data_track[0]["cost_share"]!="no"){
			$("input[name=sharing]").prop( "checked", true );
		};
		$("select[name=sharetype]").val(data_track[0]["cost_type"]);
	 	if(data_track[0]["waiver"]){
			$("input[name=waiver]").prop( "checked", true );
		};
		$("textarea[name=highlights]").val(data_track[0]["highlights"]); 
	}



	$( ".school" ).change(function() {
		 writeChange($(this),$("#school_other"));
	});


	$( ".academic" ).change(function() {
		 writeChange($(this),$("#academic_other"));
	});

	$( ".contact" ).change(function() {
		 writeChange($(this),$("#contact_other"));
	});

	$( ".gco" ).change(function() {
		 writeChange($(this),$("#gco_other"));
	});

	$( ".doctype" ).change(function() {
		 writeChange($(this),$("#doctype_other"));
	});


	$( ".sharetype" ).change(function() {
		 writeChange($(this),$("#sharetype_other"));
	});

	$(".view").click(function(){

		if($("input[name=falink]").prop("url").contains("http")){
    		window.open($("input[name=falink]").prop("url"), '_blank');
    	} else{
    		window.open("http://"+$("input[name=falink]").prop("url"), '_blank');
    	}
	});


});
