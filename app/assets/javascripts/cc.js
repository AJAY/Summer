$(document).ready(function() {
  
  $('#budget input[type=text]').addClass('input-large budget_inside');

  $(".sponsor").autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "/autocomplete.json",
          data: {
            q: request.term
          },
          success: function( data ) {
            console.log(data);
            response( data );
          }
        });
      },
      minLength: 3
  });


  $.formUtils.addValidator({
    name : 'other_value',
    validatorFunction : function(value, $el, config, language, $form) {
      if($el.prev('select').val()=="other"){
        return value.length != 0;
      }else{
        return true;
      }
    },
    errorMessage : 'You have to provide a require field',
    errorMessageKey: 'badOtherField'
  });

  $.formUtils.addValidator({
    name : 'validate_100',
    validatorFunction : function(value, $el, config, language, $form) {
      sum = 0;
      $('input[name="allocation[]"]').each(function(){
        sum += parseInt($(this).val());
      });
      if(sum==100){
        $('input[name="allocation[]"]').addClass('valid');
        $('input[name="allocation[]"]').removeAttr("style");
        $('input[name="allocation[]"]').removeClass('error');        
      }else{
        $('input[name="allocation[]"]').removeClass('valid');
        $('input[name="allocation[]"]').addClass('error');        
      }
      return sum==100;
    },
    errorMessage : 'You have to make sum of allocation to 100',
    errorMessageKey: 'badAllocation'
  });

  window.applyValidation = function() {
    $.validate({
      modules : 'date , file',
      onSuccess : function() {
        $('#intake_form').waitMe({
          effect: 'bounce',
          text: 'Please waiting...',
          bg: 'rgba(255,255,255,0.7)',
          color:'#000'
        });
       }
    });
  }  
  
  window.applyValidation();  
  
  $("#due_Date").datepicker({
      dateFormat: 'mm-dd-yy',
      minDate: '0',
      onSelect: function(selected) {
        $("#due_date").datepicker("option","minDate", selected);
      }
  });

  $("#fromDate,#toDate,#udeadline,#sdeadline").datepicker({
      dateFormat: 'mm-dd-yy'
  });

  $("#startDate").datepicker({
      dateFormat: 'mm-dd-yy',
      minDate: '0',
      onSelect: function(selected) {
        $("#endDate").datepicker("option","minDate", selected);
      }
  });

  $("#endDate").datepicker({
      dateFormat: 'mm-dd-yy',
      minDate: '0',
      onSelect: function(selected) {
        $("#startDate").datepicker("option","maxDate", selected);
      }
  })

	$(".clear_radio").click(function(){
    $(this).siblings("input[type=radio]").prop('checked', false).change();
	});

  $(".budget").change(function(){
    if($('input[name=budget]:checked').val()=="yes")
    {
      $("#budget").css({"display":"block", "margin-top":"10px"});
    }
    else
    {
       $("#budget").css("display", "none");
    }
    $('#budget input[type=text]').val("");
    $("#budget textarea").val(""); 
   
  });

  $(".conflict").change(function(){
    if($('input[name=conflict]:checked').val()=="yes")
    {
      $("#conflict").css({"display":"block", "margin-top":"10px"});
    }
    else
    {
       $("#conflict").css("display", "none");
    }
    $("input[name=conflict_person]").val("");
  });


  // $(".sponsor").change(function() {
  //     writeChange($(this),$("#sponsor_other"));
  // });

  $('#tab_logic').on('change', '.center , .collaborator', function(){
    // do something
    if($(this).val()=="other") {
      $(this).next('input').css({"display":"block", "margin-top":"10px"});
    }
    else {
      $(this).next('input').css("display", "none");
    }
    $(this).next('input').val("");
  });

  $(".costshare").change(function(){
    if($('input[name=costshare]:checked').val()=="yes") {
      $("#funding").css({"display":"block", "margin-top":"10px"});
    }
    else {
      $("#funding").css("display", "none");
    }
    $("#funding").val("");
  });

  $('#tab_logic').on( "focusout", 'input[name="allocation[]"]', function(){
      window.applyValidation();  
  });
    
  $(".funding_check").change(function(){
    if($('input[name=funding_option]:checked').val()=="provide_link")
    {
      $("#remote_url").css({"display":"block", "margin-top":"10px"});
      $("#file_upload").css("display", "none");
      $("#remote_url").attr("data-validation", "required");
      $("#file_upload_input").removeAttr("data-validation");
      $('#file_upload_input').replaceWith($('#file_upload_input').val('').clone());
      $("#uploaded").css("display", "none");
    }
    else if($('input[name=funding_option]:checked').val()=="attach_file")
    {
      $("#file_upload").css({"display":"block", "margin-top":"10px"});
      $("#remote_url").css("display", "none");
      $("#file_upload_input").attr("data-validation", "mime size required");
      $("#file_upload_input").attr("data-validation-allowing", "jpg, png, pdf, gif, zip" );
      $("#file_upload_input").attr("data-validation-max-size", "3M");
      $("#remote_url").removeAttr("data-validation");
      $("#remote_url").val("");
    }
    else{
      $("#file_upload").css("display", "none");
      $("#remote_url").css("display", "none");
      $("#file_upload_input").removeAttr("data-validation");
      $("#remote_url").removeAttr("data-validation");
      $('#file_upload_input').replaceWith($('#file_upload_input').val('').clone());
      $("#uploaded").css("display", "none");
      $("#remote_url").val("");
    }
  });

  $("#add_row").click(function(){     
    var row = "<td> CO-PI</td><td><select class='form-control collaborator' name='collaborator[]'>"
    + "<option value= ''></option>";
    
    $.each(person, function(key,val){
      row += "<option value='"+ val + "'>" + val +"</option>";
    }) 

    row += "<option value= 'other'>Other</option>";
    row += "</select><input type='text' name='collaborator_text[]' style='display:none;'"
    + "placeholder='Name' class='form-control collaborator_name'/></td><td><select " 
    + "class='form-control center' name='center[]'> <option value= ''></option> ";

    $.each(center, function(key,val){
      row += "<option value='"+ val + "'>" + val +"</option>";
    }) 

    row += "<option value= 'other'>Other</option>"
    row += "</select><input type='text' name='center_text[]' style='display:none;'"
    + "placeholder='Center' class='form-control collaborator_center'/></td>"
    + "<td><input name='allocation[]' type='text' class='input-small form-control' placeholder='Allocation %' data-validation='validate_100' "
    + "></td><td><a class='pull-right btn btn-danger delete_row'>Delete</a>"
    +"</td>";
    $('#tab_logic').append('<tr>'+row+'</tr>');
  });

  $("body").on('click', '.delete_row', function() {
    $(this).closest ('tr').remove ();
  });


  
  
  $("#add_clrow").click(function(){

    var row = "<td rowspan='4'><li></li></td> <td>Name</td><td><input name='external_name[]'"
    + " type='text' placeholder='Name' class='form-control input-md'  /> </td><td rowspan='4'> "
    + " <a class='pull-right btn btn-danger delete_box'> Delete</a></td></tr><tr> <td> Contact </td>"
    + " <td> <input type='text' name='external_contact[]' placeholder='Contact' class='form-control'/> "
    + "</td> </tr> <tr> <td> Which Institution is the Lead? </td> <td>"
    + " <input type='text' name='external_institution[]' placeholder='Institution Name'" 
    + "class='form-control'/> </td> </tr> <tr> <td> Name of Lead PI at collaborating institutions? </td>" 
    + "<td> <input type='text' name='external_PI[]' placeholder='Name of Lead PI' class='form-control'/>"
    + "</td> </tr>"; 
    $('#tab_col').append('<tr>'+row+'</tr>');
  });
        
  $("body").on('click', '.delete_box', function() {
    $(this).closest ('tr').next().remove ();
    $(this).closest ('tr').next().remove ();
    $(this).closest ('tr').next().remove ();
    $(this).closest ('tr').remove ();
  });

  $("#select_all").click(function(){
    $("input[type='checkbox']").prop("checked", true);
  });
 
  $("#deselect_all").click(function(){
    $("input[type='checkbox']").prop("checked", false);
  });
  
  $("#reset").click(function(){
    $(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('').change();
    $(':checkbox, :radio').prop('checked', false).change();
  }); 

});

function writeChange (i, elem) {
  if(i.val()=="other") {
    elem.css({"display":"block", "margin-top":"10px"});
  }
  else {
     elem.css("display", "none");
  }
  elem.val("");   
}
