class CreateBudgets < ActiveRecord::Migration
  def change
    create_table :budgets do |t|
 	  	t.string 	 :budget_option
      	t.belongs_to :project, index: true
      	t.string	 :total_target
      	t.string	 :annual_target
      	t.string	 :pi_co_effort
      	t.string	 :grad_student
		t.string	 :post_docs
		t.string	 :other_staff
		t.string	 :material_supplies
		t.string  	 :equipment_fees
		t.string  	 :consultants
		t.string	 :subgrant
		t.string  	 :publications
		t.string	 :foreign_travel
		t.string	 :domestic_travel
		t.text		 :domestic_description
		t.string 	 :non_capital_equipment
		t.text		 :non_capital_description
		t.text		 :other
      	t.timestamps null: false
    end

  end
end
