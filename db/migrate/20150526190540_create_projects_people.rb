class CreateProjectsPeople < ActiveRecord::Migration
  def change
    create_table :invites, :id=>false do |t|
    	t.belongs_to :project, index: true
      	t.belongs_to :person, index: true
	    t.string :allocation
      	t.boolean :deleted, :default => false
      	t.timestamps null: false
    end
  end
end
