class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
	 	t.string  :name
	    t.string  :contact
	    t.string  :institution
        t.string  :leadPI
        t.string  :category
        t.string  :center
		t.boolean :deleted, :default => false
		t.timestamps null: false
    end

    add_index :people, [:name, :category, :center], :unique => true	
  end
end
