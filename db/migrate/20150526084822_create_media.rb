class CreateMedia < ActiveRecord::Migration
  def change
    create_table :media do |t|
 	    t.string      :option
      t.string      :filename
      t.string      :content_type
      t.string      :url
      t.text        :description
      t.belongs_to :project, index: true 
      t.boolean     :deleted, :default => false
      t.timestamps null: false
    end
  end
end
