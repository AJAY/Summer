class CreateProjects < ActiveRecord::Migration
  def change
    create_table   :projects do |t|
	    t.string   :title
	    t.date 	   :due_at
	    t.date     :start_date
	    t.date     :end_date
	    t.string   :sponsor
		t.string   :cost_share
		t.string   :human_subject
		t.string   :clinical_trials
		t.string   :animals
		t.string   :waste
		t.string   :ip_issues
		t.string   :conflict_person
		t.string   :RA_name
		t.string   :proposal_status
		t.string   :award_status
		t.string   :solicitation
		t.string   :routing
		t.string   :fastlane
		t.text	   :additional_notes
		t.string   :proposal_no
		t.string   :school
		t.string   :academic
		t.string   :prime_sponsor
		t.string   :gco
		t.string   :account_no
		t.string   :cancellation_reason
		t.string   :document_type
		t.string   :notification
		t.date     :unitdeadline
		t.string   :cost_type
		t.boolean  :waiver
		t.text	   :highlights
		t.string   :lock_version
		t.string   :RA_contact
        t.timestamps null: false
    end

    add_index	:projects, :start_date
    add_index	:projects, :end_date
    add_index   :projects, :created_at
    add_index   :projects, :updated_at
  end
end
