# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150629031246) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "budgets", force: :cascade do |t|
    t.string   "budget_option",           limit: 255
    t.integer  "project_id",              limit: 4
    t.string   "total_target",            limit: 255
    t.string   "annual_target",           limit: 255
    t.string   "pi_co_effort",            limit: 255
    t.string   "grad_student",            limit: 255
    t.string   "post_docs",               limit: 255
    t.string   "other_staff",             limit: 255
    t.string   "material_supplies",       limit: 255
    t.string   "equipment_fees",          limit: 255
    t.string   "consultants",             limit: 255
    t.string   "subgrant",                limit: 255
    t.string   "publications",            limit: 255
    t.string   "foreign_travel",          limit: 255
    t.string   "domestic_travel",         limit: 255
    t.text     "domestic_description",    limit: 65535
    t.string   "non_capital_equipment",   limit: 255
    t.text     "non_capital_description", limit: 65535
    t.text     "other",                   limit: 65535
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "budgets", ["project_id"], name: "index_budgets_on_project_id", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "invites", id: false, force: :cascade do |t|
    t.integer  "project_id", limit: 4
    t.integer  "person_id",  limit: 4
    t.string   "allocation", limit: 255
    t.boolean  "deleted",    limit: 1,   default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "invites", ["person_id"], name: "index_invites_on_person_id", using: :btree
  add_index "invites", ["project_id"], name: "index_invites_on_project_id", using: :btree

  create_table "media", force: :cascade do |t|
    t.string   "option",       limit: 255
    t.string   "filename",     limit: 255
    t.string   "content_type", limit: 255
    t.string   "url",          limit: 255
    t.text     "description",  limit: 65535
    t.integer  "project_id",   limit: 4
    t.boolean  "deleted",      limit: 1,     default: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "media", ["project_id"], name: "index_media_on_project_id", using: :btree

  create_table "people", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "contact",     limit: 255
    t.string   "institution", limit: 255
    t.string   "leadPI",      limit: 255
    t.string   "category",    limit: 255
    t.string   "center",      limit: 255
    t.boolean  "deleted",     limit: 1,   default: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "people", ["name", "category", "center"], name: "index_people_on_name_and_category_and_center", unique: true, using: :btree

  create_table "projects", force: :cascade do |t|
    t.string   "title",               limit: 255
    t.date     "due_at"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "sponsor",             limit: 255
    t.string   "cost_share",          limit: 255
    t.string   "human_subject",       limit: 255
    t.string   "clinical_trials",     limit: 255
    t.string   "animals",             limit: 255
    t.string   "waste",               limit: 255
    t.string   "ip_issues",           limit: 255
    t.string   "conflict_person",     limit: 255
    t.string   "RA_name",             limit: 255
    t.string   "proposal_status",     limit: 255
    t.string   "award_status",        limit: 255
    t.string   "solicitation",        limit: 255
    t.string   "routing",             limit: 255
    t.string   "fastlane",            limit: 255
    t.text     "additional_notes",    limit: 65535
    t.string   "proposal_no",         limit: 255
    t.string   "school",              limit: 255
    t.string   "academic",            limit: 255
    t.string   "prime_sponsor",       limit: 255
    t.string   "gco",                 limit: 255
    t.string   "account_no",          limit: 255
    t.string   "cancellation_reason", limit: 255
    t.string   "document_type",       limit: 255
    t.string   "notification",        limit: 255
    t.date     "unitdeadline"
    t.string   "cost_type",           limit: 255
    t.boolean  "waiver",              limit: 1
    t.text     "highlights",          limit: 65535
    t.string   "lock_version",        limit: 255
    t.string   "RA_contact",          limit: 255
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "projects", ["created_at"], name: "index_projects_on_created_at", using: :btree
  add_index "projects", ["end_date"], name: "index_projects_on_end_date", using: :btree
  add_index "projects", ["start_date"], name: "index_projects_on_start_date", using: :btree
  add_index "projects", ["updated_at"], name: "index_projects_on_updated_at", using: :btree

  create_table "sponsors", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "role",                   limit: 255
    t.integer  "failed_attempts",        limit: 4,   default: 0
    t.string   "unlock_token",           limit: 255
    t.datetime "locked_at"
    t.integer  "cached_failed_attempts", limit: 4,   default: 0
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

end
